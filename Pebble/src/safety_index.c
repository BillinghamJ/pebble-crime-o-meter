#include "pebble_os.h"
#include "pebble_app.h"
#include "pebble_fonts.h"

#define MY_UUID { 0x39, 0xF3, 0xFA, 0x5E, 0x2E, 0xDB, 0x42, 0x44, 0x8E, 0xEC, 0x20, 0x17, 0x5B, 0x4C, 0x51, 0x5D }

PBL_APP_INFO(
	MY_UUID,
	"Safety Index", "MVP Tracker LLC",
	1, 0, /* App version */
	DEFAULT_MENU_ICON,
	APP_INFO_STANDARD_APP);

Window window;
TextLayer text_layer;

void handle_init(AppContextRef ctx) {
	window_init(&window, "Safety Index");
	window_stack_push(&window, true);

	text_layer_init(&text_layer, GRect(0, 0, window.layer.frame.size.w, window.layer.frame.size.h - 15));
	text_layer_set_text_alignment(&text_layer, GTextAlignmentCenter);
	text_layer_set_text(&text_layer, "Waiting...");
	text_layer_set_font(&text_layer, fonts_get_system_font(FONT_KEY_ROBOTO_CONDENSED_21));
	layer_add_child(&window.layer, &text_layer.layer);
}

void receive(DictionaryIterator *received, void *context) {
	char* action = dict_find(received, 0)->value->cstring;

	if (strcmp(action, (char*)"crime_update") == 0) {
		text_layer_set_text(&text_layer, dict_find(received, 1)->value->cstring);

		if (strcmp(dict_find(received, 2)->value->cstring, (char*)"1") == 0)
			vibes_long_pulse();
	}
}

void pbl_main(void *params) {
	PebbleAppHandlers handlers = {
		.init_handler = &handle_init,
		.messaging_info = {
			.buffer_sizes = {
				.inbound = 512,
				.outbound = 512
			},
			.default_callbacks.callbacks = {
				.in_received = receive
			}
		}
	};

	app_event_loop(params, &handlers);
}
