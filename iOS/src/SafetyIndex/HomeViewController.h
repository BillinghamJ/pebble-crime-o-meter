//
//  HomeViewController.h
//  SafetyIndex
//
//  Created by James Billingham on 10/08/2013.
//  Copyright (c) 2013 GoPebblr. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeViewController : UIViewController

- (IBAction)buttonTapped:(id)sender;

@end
