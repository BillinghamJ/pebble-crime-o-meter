//
//  AppDelegate.m
//  FourPebbSq
//
//  Created by James Billingham on 22/06/2013.
//  Copyright (c) 2013 GoPebblr. All rights reserved.
//

#import "AppDelegate.h"
#import <PebbleKit/PebbleKit.h>
#import <CoreLocation/CoreLocation.h>
#import "KBPebbleMessageQueue.h"

@interface AppDelegate () <PBPebbleCentralDelegate, CLLocationManagerDelegate>
{
  CLLocationManager* _locationManager;
	KBPebbleMessageQueue* _messageQueue;
	NSInteger _lastCrimeNumber;
	bool _active;
}
@end

@implementation AppDelegate

- (BOOL) application:(UIApplication*)application didFinishLaunchingWithOptions:(NSDictionary*)launchOptions
{
	UIApplication.sharedApplication.idleTimerDisabled = YES;
	
	_lastCrimeNumber = 0;
	
	_locationManager = [[CLLocationManager alloc] init];
  _locationManager.distanceFilter = 50.0;
  _locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters;
  _locationManager.delegate = self;
	
	_messageQueue = [[KBPebbleMessageQueue alloc] init];
	_messageQueue.watch = PBPebbleCentral.defaultCentral.lastConnectedWatch;
	
	[PBPebbleCentral.defaultCentral.lastConnectedWatch appMessagesGetIsSupported:^(PBWatch *watch, BOOL isAppMessagesSupported) {
		if (!isAppMessagesSupported)
		{
			[[[UIAlertView alloc] initWithTitle:@"Error" message:@"Pebble doesn't support AppMessages" delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil] show];
			return;
		}
		
		uint8_t uuid[] = { 0x39, 0xF3, 0xFA, 0x5E, 0x2E, 0xDB, 0x42, 0x44, 0x8E, 0xEC, 0x20, 0x17, 0x5B, 0x4C, 0x51, 0x5D };
		[watch appMessagesSetUUID:[NSData dataWithBytes:uuid length:sizeof(uuid)]];
		[_locationManager startUpdatingLocation];
		_active = true;
	}];
	
	return true;
}

- (void) locationManager:(CLLocationManager*)manager didUpdateLocations:(NSArray*)locations
{
	CLLocationCoordinate2D coords = manager.location.coordinate;
	[self newLocation:coords];
}

- (void) overrideLocation
{
	if (_active)
	{
		[_locationManager stopUpdatingLocation];
		_locationManager.delegate = nil;
		
		[self newLocation:CLLocationCoordinate2DMake(51.462745829977855, -0.11456409841776)];
	}
	else
	{
		[_locationManager startUpdatingLocation];
		_locationManager.delegate = self;
	}
	
	_active = !_active;
}

- (void) newLocation:(CLLocationCoordinate2D)coords
{
	NSInteger crimeNumber = [self getCrimeWithCoordinates:coords];
	
	if (crimeNumber == -1)
	{
		NSLog(@"Unable to locate current neighbourhood - %f,%f", coords.latitude, coords.longitude);
		return;
	}
	
	NSLog(@"%d", crimeNumber);
	
	NSString* crimeInfo = (crimeNumber > 10 ? @"High Crime" : @"Low Crime");
	NSString* output = [NSString stringWithFormat:@"%f\n%f\n\n%d\n\n%@", coords.latitude, coords.longitude, crimeNumber, crimeInfo];
	
	bool shouldVibrate = crimeNumber > 10;
	
	if (_lastCrimeNumber > 10)
		shouldVibrate = false;
	
	NSString* vibrate = shouldVibrate ? @"1" : @"0";
	
	_lastCrimeNumber = crimeNumber;
	
	NSDictionary* message =
	[[NSDictionary alloc] initWithObjectsAndKeys:
		@"crime_update", @(0),
		output, @(1),
		vibrate, @(2),
		nil];
	
	[_messageQueue enqueue:message];
}

- (NSInteger) getCrimeWithCoordinates:(CLLocationCoordinate2D)coords
{
	NSString* url = [NSString stringWithFormat:@"http://data.police.uk/api/crimes-at-location?date=2013-06&lat=%f&lng=%f", coords.latitude, coords.longitude];
	NSData* data = [NSData dataWithContentsOfURL:[NSURL URLWithString:url]];
	
	NSError* error;
	NSArray* json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
	
	if (error || json == nil)
		return -1;
	
	return json.count;
}

- (void) pebbleCentral:(PBPebbleCentral*)central watchDidConnect:(PBWatch*)watch isNew:(BOOL)isNew
{
	_messageQueue.watch = watch;
	
	[watch appMessagesGetIsSupported:^(PBWatch* watch, BOOL isAppMessagesSupported)
	{
		if (!isAppMessagesSupported)
		{
			[[[UIAlertView alloc] initWithTitle:@"Error" message:@"Pebble doesn't support AppMessages" delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil] show];
			return;
		}
		
		uint8_t uuid[] = { 0x39, 0xF3, 0xFA, 0x5E, 0x2E, 0xDB, 0x42, 0x44, 0x8E, 0xEC, 0x20, 0x17, 0x5B, 0x4C, 0x51, 0x5D };
		[watch appMessagesSetUUID:[NSData dataWithBytes:uuid length:sizeof(uuid)]];
		[_locationManager startUpdatingLocation];
	}];
}

- (void) pebbleCentral:(PBPebbleCentral*)central watchDidDisconnect:(PBWatch*)watch
{
	[[[UIAlertView alloc] initWithTitle:@"Disconnected!" message:[watch name] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
	
	if (_messageQueue.watch == watch || [watch isEqual:_messageQueue.watch])
		_messageQueue.watch = nil;
}

@end
